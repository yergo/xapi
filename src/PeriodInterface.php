<?php
declare(strict_types=1);

namespace Yergo\Xapi;

interface PeriodInterface
{
    public const PERIOD_M1 = 1;
    public const PERIOD_M5 = 5;
    public const PERIOD_M15 = 15;
    public const PERIOD_M30 = 30;
    public const PERIOD_H1 = 60;
    public const PERIOD_H4 = 240;
    public const PERIOD_D1 = 1440;
    public const PERIOD_W1 = 10080;
    public const PERIOD_MN1 = 43200;

}
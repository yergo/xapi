<?php
declare(strict_types=1);

namespace Yergo\Xapi;

use Yergo\Xapi\ApiCommand\GetChartLastRequest;
use Yergo\Xapi\ApiCommand\GetChartRangeRequest;
use Yergo\Xapi\ApiCommand\GetCommissionDef;
use Yergo\Xapi\ApiCommand\GetIbsHistory;
use Yergo\Xapi\ApiCommand\GetMarginTrade;
use Yergo\Xapi\ApiCommand\GetNews;
use Yergo\Xapi\ApiCommand\GetProfitCalculation;
use Yergo\Xapi\ApiCommand\GetSymbol;
use Yergo\Xapi\ApiCommand\GetTickPrices;
use Yergo\Xapi\ApiCommand\GetTradeRecords;
use Yergo\Xapi\ApiCommand\GetTrades;
use Yergo\Xapi\ApiCommand\GetTradesHistory;
use Yergo\Xapi\ApiCommand\GetTradingHours;
use Yergo\Xapi\ApiCommand\TradeTransaction;
use Yergo\Xapi\ApiCommand\TradeTransactionStatus;
use Yergo\Xapi\ApiCommand\Login;

/**
 * Class CommandFactory
 *
 * @package Yergo\Xapi
 *
 * @method static Login login($userId, $password)
 * @method static Command logout()
 * @method static Command getCalendar()
 * @method static Command getAllSymbols()
 * @method static GetChartLastRequest getChartLastRequest(LastInfoRecord $info)
 * @method static GetChartRangeRequest getChartRangeRequest(RangeInfoRecord $info)
 * @method static GetCommissionDef getCommissionDef(RangeInfoRecord $info)
 * @method static Command getCurrentUserData()
 * @method static GetIbsHistory getIbsHistory(\DateTime $start, \DateTime $end)
 * @method static Command getMarginLevel()
 * @method static GetMarginTrade getMarginTrade(string $symbol, float $volume)
 * @method static GetNews getNews(\DateTime $start, \DateTimeZone $end)
 * @method static GetProfitCalculation getProfitCalculation
 * @method static Command getServerTime()
 * @method static Command getStepRules()
 * @method static GetSymbol getSymbol(string $symbol)
 * @method static GetTickPrices getTickPrices(array $symbols, \DateTime $time, int $level = 0)
 * @method static GetTradeRecords getTradeRecords(int ...$orderId)
 * @method static GetTrades getTrades(bool $openedOnly)
 * @method static GetTradesHistory getTradesHistory(\DateTime $start, \DateTimeZone $end)
 * @method static GetTradingHours getTradingHours(string ...$symbols)
 * @method static Command getVersion()
 * @method static Command ping()
 * @method static TradeTransaction tradeTransaction(Transaction $transaction)
 * @method static TradeTransactionStatus tradeTransactionStatus(int $orderId)
 */
class CommandFactory
{
    public static function __callStatic( $name, $params )
    {
        switch ( $name ) {
            case 'logout':
            case 'getCalendar':
            case 'getAllSymbols':
            case 'getCurrentUserData':
            case 'getMarginLevel':
            case 'getServerTime':
            case 'getStepRules':
            case 'getVersion':
            case 'ping':
                return new Command( $name );
            default:
                break;
        }

        $name = ucfirst( $name );
        $name = "Yergo\\Xapi\\ApiCommand\\{$name}";
        if ( ! class_exists( $name ) ) {
            throw new \RuntimeException( 'Class does not exist: ' . $name );
        }

        return new $name( ...$params );
    }
}
<?php
declare(strict_types=1);

namespace Yergo\Xapi;

class StreamClient extends JsonSocketAbstract
{
    public static int $_BLOCK = 10000;

    protected int $port = 5125;

    protected string $streamSessionId;

    protected array $tickers = [];

    const TICK_BALANCE = 'balance';
    const TICK_CANDLE = 'candle';
    const TICK_KEEP_ALIVE = 'keepAlive';
    const TICK_NEWS = 'news';
    const TICK_PROFIT = 'profit';
    const TICK_PRICE = 'tickPrices';
    const TICK_TRADE = 'trade';
    const TICK_TRADE_STATUS = 'tradeStatus';

    const TICKS_AVAILABLE = [
        self::TICK_BALANCE,
        self::TICK_CANDLE,
        self::TICK_KEEP_ALIVE,
        self::TICK_NEWS,
        self::TICK_PRICE,
        self::TICK_PROFIT,
        self::TICK_TRADE,
        self::TICK_TRADE_STATUS,
    ];

    public function __construct(string $streamSessionId)
    {
        $this->streamSessionId = $streamSessionId;
    }

    public function setTicker( $tickName, callable $callback)
    {
        if (in_array($tickName, self::TICKS_AVAILABLE)) {
            $this->tickers[$tickName][] = $callback;
        }
    }

    public function subscribe( $tickName, ?string $symbol = null )
    {
        $command = '';
        $extra = null;

        switch($tickName) {
            case self::TICK_BALANCE:
                $command = 'getBalance';
                break;
            case self::TICK_CANDLE:
                $command = 'getCandles';
                $extra = ['symbol' => $symbol];
                break;
            case self::TICK_KEEP_ALIVE:
                $command = 'getKeepAlive';
                break;
            case self::TICK_NEWS:
                $command = 'getNews';
                break;
            case self::TICK_PRICE:
                $command = 'getTickPrices';
                $extra = ['symbol' => $symbol];
                break;
            case self::TICK_PROFIT:
                $command = 'getProfits';
                break;
            case self::TICK_TRADE:
                $command = 'getTrades';
                break;
            case self::TICK_TRADE_STATUS:
                $command = 'getTradeStatus';
                break;

        }

        $cmd = new Command($command);
        if ($extra) {
            foreach ($extra as $property => $value) {
                $cmd = $cmd->withExtra($property, $value);
            }
        }

        $this->send($cmd);
    }

    public function unsubscribe( $tickName, ?string $symbol = null )
    {
        $command = '';
        $extra = null;

        switch($tickName) {
            case self::TICK_BALANCE:
                $command = 'stopBalance';
                break;
            case self::TICK_CANDLE:
                $command = 'stopCandles';
                $extra = ['symbol' => $symbol];
                break;
            case self::TICK_KEEP_ALIVE:
                $command = 'stopKeepAlive';
                break;
            case self::TICK_NEWS:
                $command = 'stopNews';
                break;
            case self::TICK_PRICE:
                $command = 'stopTickPrices';
                $extra = ['symbol' => $symbol];
                break;
            case self::TICK_PROFIT:
                $command = 'stopProfits';
                break;
            case self::TICK_TRADE:
                $command = 'stopTrades';
                break;
            case self::TICK_TRADE_STATUS:
                $command = 'stopTradeStatus';
                break;

        }

        $cmd = new Command($command);
        if ($extra) {
            foreach ($extra as $property => $value) {
                $cmd = $cmd->withExtra($property, $value);
            }
        }

        $this->send($cmd);
    }

    public function listen()
    {
        $lastTick = microtime(true);
        while($this->socket) {
            $feed = $this->read();
            $currentTick = microtime(true);

            if (!$feed) {
                if ($currentTick - $lastTick > 60*8) {
                    $this->send(CommandFactory::ping());
                }

            } else {
                if (isset($this->tickers[$feed['command']])) {
                    foreach ($this->tickers[$feed['command']] as $ticker) {
                        $ticker( $feed[ 'data' ] ?? null );
                    }
                }
            }

            $lastTick = microtime(true);
        }
    }

    /**
     * Nonblocking read
     *
     * @return array|null
     * @throws \ErrorException
     */
    protected function read(): ?array
    {
        $out = stream_get_line( $this->socket, 2048, "\n" );
        if ( ! $out ) {
            if (self::$_BLOCK > 0) {
                usleep(self::$_BLOCK);
            }
            return null;
        }
        $data = json_decode( trim( $out ), true );

        if ( isset($data[ 'command' ]) ) {
            return $data;
        } else {
            throw new \ErrorException( $data );
        }
    }

    protected function cmdToJson( CommandInterface $command ): string
    {
        return json_encode($command->withExtra('streamSessionId', $this->streamSessionId));
    }
}
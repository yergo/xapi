<?php
declare(strict_types=1);

namespace Yergo\Xapi;

class ApiClient extends JsonSocketAbstract
{
    protected int $port = 5124;

    public function execute( CommandInterface $command ): ?array
    {
        $this->send( $command );
        return $this->read( true );
    }

}
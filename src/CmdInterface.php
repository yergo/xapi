<?php
declare(strict_types=1);

namespace Yergo\Xapi;

interface CmdInterface extends \JsonSerializable
{
    public const BUY = 0;
    public const SELL = 1;
    public const BUY_LIMIT = 2;
    public const SELL_LIMIT = 3;
    public const BUY_STOP = 4;
    public const SELL_STOP = 5;
    public const BALANCE = 6;
    public const CREDIT = 7;
}
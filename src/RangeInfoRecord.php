<?php
declare(strict_types=1);

namespace Yergo\Xapi;

class RangeInfoRecord implements \JsonSerializable, PeriodInterface
{

    protected int $period;
    protected string $symbol;
    protected int $start;
    protected int $end;
    protected int $ticks = 0;

    public function __construct(
        string $symbol,
        int $period = self::PERIOD_M1,
        ?\DateTime $start = null,
        ?\DateTime $end = null,
        int $ticks = 0
    ) {
        $this->period = $period;
        $this->symbol = $symbol;

        $tz = new \DateTimeZone('Europe/Berlin');

        $timestamp = ($start ?? new \DateTime('now', $tz))->setTimezone($tz)->getTimestamp();
        $this->start = $timestamp * 1000;

        $timestamp = ($end ?? new \DateTime('now', $tz))->setTimezone($tz)->getTimestamp();
        $this->end = $timestamp * 1000;

        $this->ticks = $ticks;
    }

    public function jsonSerialize(  )
    {
        return get_object_vars($this);
    }
}
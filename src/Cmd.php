<?php
declare(strict_types=1);

namespace Yergo\Xapi;

/**
 * Class Cmd
 *
 * @package Yergo\Xapi
 *
 * @method static CmdInterface BUY()
 * @method static CmdInterface SELL()
 * @method static CmdInterface BUY_LIMIT()
 * @method static CmdInterface SELL_LIMIT()
 * @method static CmdInterface BUY_STOP()
 * @method static CmdInterface SELL_STOP()
 * @method static CmdInterface BALANCE()
 * @method static CmdInterface CREDIT()
 */
class Cmd implements CmdInterface
{
    protected int $cmd;

    public function __construct(int $cmd = self::BUY)
    {
        $this->cmd = $cmd;
    }

    public static function __callStatic( $name, $args )
    {
        return new self(self::$$name);
    }

    public function jsonSerialize()
    {
        return $this->cmd;
    }


}
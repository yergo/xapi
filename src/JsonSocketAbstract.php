<?php
declare(strict_types=1);

namespace Yergo\Xapi;

abstract class JsonSocketAbstract
{
    public static int $_BLOCK = 10000;

    protected string $host = 'xapi.xtb.com';

    protected int $port;

    /**
     * @var resource
     */
    protected $socket;

    public function connect()
    {
        $ip = gethostbyname( $this->host );

        $context = stream_context_create();
        stream_context_set_option( $context, 'ssl', 'verify_peer', false );
        stream_context_set_option( $context, 'ssl', 'verify_peer_name', false );

        $socket = stream_socket_client(
            "ssl://{$ip}:{$this->port}",
            $errno,
            $errstr,
            1,
            STREAM_CLIENT_CONNECT,
            $context
        );

        stream_set_blocking( $socket, false );

        if ( ! $socket ) {
            echo("Error: " . $errstr);
            exit();
        }

        $this->socket = $socket;
    }

    protected function send( CommandInterface $command )
    {
        $jsonCmd = $this->cmdToJson( $command );

        $cmdLen = strlen( $jsonCmd );
        $result = fwrite( $this->socket, $jsonCmd );
        if ( $result < $cmdLen ) {
            throw new \RuntimeException( 'Error sending command ' . $jsonCmd );
        }
    }

    protected function cmdToJson( CommandInterface $command ): string
    {
        return json_encode( $command );
    }

    /**
     * blocking read
     *
     * @return array|null
     * @throws \ErrorException
     */
    protected function read(): ?array
    {
        while ( true ) {
            $out = stream_get_line( $this->socket, 1024*1024, "\n" );
            if ( ! $out ) {
                if (self::$_BLOCK > 0) {
                    usleep(self::$_BLOCK);
                }
                continue;
            }
            $data = json_decode( trim( $out ), true );

            if ( $data[ 'status' ] === true ) {
                if ( isset( $data[ 'returnData' ] ) ) {
                    return $data[ 'returnData' ];
                } else {
                    unset( $data[ 'status' ] );
                    return $data;
                }
            } else {
                throw new \ErrorException( $data[ 'errorDescr' ], (int) $data[ 'errorCode' ] );
            }

        }
    }

    public function __destruct()
    {
        if ( $this->socket ) {
            fclose( $this->socket );
            $this->socket = null;
        }
    }
}
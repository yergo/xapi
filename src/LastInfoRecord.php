<?php
declare(strict_types=1);

namespace Yergo\Xapi;

class LastInfoRecord implements \JsonSerializable, PeriodInterface
{

    protected int $period;
    protected string $symbol;
    protected int $start;

    public function __construct(
        string $symbol,
        int $period = self::PERIOD_M1,
        ?\DateTime $start = null
    ) {
        $this->period = $period;
        $this->symbol = $symbol;

        $tz = new \DateTimeZone('Europe/Berlin');
        $timestamp = ($start ?? new \DateTime( 'now', $tz))->setTimezone( $tz)->getTimestamp();

        $this->start = $timestamp * 1000;
    }

    public function jsonSerialize(  )
    {
        return get_object_vars($this);
    }
}
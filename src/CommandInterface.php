<?php
declare(strict_types=1);


namespace Yergo\Xapi;


interface CommandInterface extends \JsonSerializable
{
    public function withExtra( $name, $value ): self;
}
<?php
declare(strict_types=1);

namespace Yergo\Xapi\ApiCommand;

use Yergo\Xapi\Command;

class GetMarginTrade extends Command
{
    public function __construct( string $symbol, float $volume )
    {
        parent::__construct( 'getMarginTrade', [
            'symbol' => $symbol,
            'volume' => $volume,
        ] );
    }
}
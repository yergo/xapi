<?php
declare(strict_types=1);

namespace Yergo\Xapi\ApiCommand;

use Yergo\Xapi\Command;

class GetTrades extends Command
{
    public function __construct( bool $openedOnly = true )
    {
        parent::__construct(
            'getTrades',
            [
                'openedOnly' => $openedOnly,
            ]
        );
    }
}
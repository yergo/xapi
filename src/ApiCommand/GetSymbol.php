<?php
declare(strict_types=1);

namespace Yergo\Xapi\ApiCommand;

use Yergo\Xapi\Command;

class GetSymbol extends Command
{
    public function __construct( string $symbol )
    {
        parent::__construct( 'getSymbol', [
            'symbol' => $symbol,
        ] );
    }
}
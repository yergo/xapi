<?php
declare(strict_types=1);

namespace Yergo\Xapi\ApiCommand;

use Yergo\Xapi\Command;

class TradeTransactionStatus extends Command
{
    public function __construct( int $orderId )
    {
        parent::__construct(
            'tradeTransactionStatus',
            [
                'order' => $orderId,
            ]
        );
    }
}
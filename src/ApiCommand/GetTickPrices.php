<?php
declare(strict_types=1);

namespace Yergo\Xapi\ApiCommand;

use Yergo\Xapi\Command;

class GetTickPrices extends Command
{
    /**
     * GetTickPrices constructor.
     *
     * @param string[]  $symbols
     * @param \DateTime $time
     * @param int       $level
     */
    public function __construct(
        array $symbols,
        \DateTime $time,
        int $level = 0
    ) {
        $tz = new \DateTimeZone( 'Europe/Berlin' );
        $time = (clone $time)->setTimezone( $tz );

        parent::__construct(
            'getTickPrices',
            [
                'level'   => $level,
                'symbols' => $symbols,
                'time'    => $time,
            ]
        );
    }
}
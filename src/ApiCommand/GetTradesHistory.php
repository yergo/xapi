<?php
declare(strict_types=1);

namespace Yergo\Xapi\ApiCommand;

use Yergo\Xapi\Command;

class GetTradesHistory extends Command
{
    public function __construct(
        \DateTime $start,
        ?\DateTimeZone $end = null
    ) {
        $tz = new \DateTimeZone('Europe/Berlin');

        $end = $end ?? new \DateTime('now', $tz);

        parent::__construct(
            'getNews',
            [
                'start' => (clone $start)->setTimezone($tz)->getTimestamp() * 1000,
                'end'   => (clone $end)->setTimezone($tz)->getTimestamp() * 1000,
            ]
        );
    }
}
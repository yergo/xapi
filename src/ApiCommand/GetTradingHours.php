<?php
declare(strict_types=1);

namespace Yergo\Xapi\ApiCommand;

use Yergo\Xapi\Command;

class GetTradingHours extends Command
{
    public function __construct( string ...$symbols )
    {
        parent::__construct( 'getTradingHours', [
            'symbols' => $symbols
        ] );
    }
}
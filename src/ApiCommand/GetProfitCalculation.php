<?php
declare(strict_types=1);

namespace Yergo\Xapi\ApiCommand;

use Yergo\Xapi\Cmd;
use Yergo\Xapi\CmdInterface;
use Yergo\Xapi\Command;

class GetProfitCalculation extends Command
{
    public function __construct(
        string $symbol,
        float $volume,
        float $openPrice,
        float $closePrice,
        ?CmdInterface $cmd = null
    ) {
        parent::__construct(
            'getProfitCalculation',
            [
                'symbol'     => $symbol,
                'volume'     => $volume,
                'openPrice'  => $openPrice,
                'closePrice' => $closePrice,
                'cmd'        => $cmd ?? Cmd::BUY(),
            ]
        );
    }
}
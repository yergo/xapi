<?php
declare(strict_types=1);

namespace Yergo\Xapi\ApiCommand;

use Yergo\Xapi\Command;
use Yergo\Xapi\RangeInfoRecord;

class GetChartRangeRequest extends Command
{
    public function __construct( ?RangeInfoRecord $info )
    {
        parent::__construct( 'getChartRangeRequest', ['info' => $info] );
    }
}
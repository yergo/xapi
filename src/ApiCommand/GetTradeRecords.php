<?php
declare(strict_types=1);

namespace Yergo\Xapi\ApiCommand;

use Yergo\Xapi\Command;

class GetTradeRecords extends Command
{
    public function __construct( int ...$orderIds )
    {
        parent::__construct(
            'getTradeRecords',
            [
                'orders' => $orderIds,
            ]
        );
    }
}
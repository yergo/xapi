<?php
declare(strict_types=1);

namespace Yergo\Xapi\ApiCommand;

use Yergo\Xapi\Command;

class GetCommissionDef extends Command
{
    public function __construct( string $symbol, float $volume )
    {
        parent::__construct( 'getCommissionDef', [
            'symbol' => $symbol,
            'volume' => $volume,
        ] );
    }
}
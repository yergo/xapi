<?php
declare(strict_types=1);


namespace Yergo\Xapi\ApiCommand;

use Yergo\Xapi\Command;

class Login extends Command
{
    public function __construct( string $userId, string $password )
    {
        parent::__construct( 'login', ['userId' => $userId, 'password' => $password] );
    }
}
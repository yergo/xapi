<?php
declare(strict_types=1);


namespace Yergo\Xapi\ApiCommand;


use Yergo\Xapi\Command;

class GetIbsHistory extends Command
{
    public function __construct( \DateTime $start, \DateTime $end )
    {
        $tz = new \DateTimeZone( 'Europe/Berlin' );
        $start = (clone $start)->setTimezone( $tz );
        $end = (clone $end)->setTimezone( $tz );
        parent::__construct(
            'getIbsHistory',
            [
                'start' => $start->getTimestamp() * 1000,
                'end'   => $end->getTimestamp() * 1000,
            ]
        );
    }
}
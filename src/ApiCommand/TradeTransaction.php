<?php
declare(strict_types=1);

namespace Yergo\Xapi\ApiCommand;

use Yergo\Xapi\Command;
use Yergo\Xapi\Transaction;

class TradeTransaction extends Command
{
    public function __construct( Transaction $transaction )
    {
        parent::__construct(
            'tradeTransaction',
            [
                'tradeTransInfo' => $transaction,
            ]
        );
    }
}
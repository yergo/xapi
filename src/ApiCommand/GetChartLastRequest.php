<?php
declare(strict_types=1);

namespace Yergo\Xapi\ApiCommand;

use Yergo\Xapi\Command;
use Yergo\Xapi\LastInfoRecord;

class GetChartLastRequest extends Command
{
    public function __construct( ?LastInfoRecord $info )
    {
        parent::__construct( 'getChartLastRequest', ['info' => $info] );
    }
}
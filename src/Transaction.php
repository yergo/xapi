<?php
declare(strict_types=1);

namespace Yergo\Xapi;

class Transaction implements \JsonSerializable
{
    public const TYPE_OPEN = 0;
    public const TYPE_PENDING = 1;
    public const TYPE_CLOSE = 2;
    public const TYPE_MODIFY = 3;
    public const TYPE_DELETE = 4;

    protected CmdInterface $cmd;
    protected string $symbol;
    protected float $volume;
    protected float $price;
    protected float $sl;
    protected float $tp;
    protected int $expiration;
    protected int $type = self::TYPE_OPEN;
    protected int $offset = 0;
    protected string $customComment = '';
    protected int $order = 0;

    public function __construct(
        CmdInterface $cmd,
        string $symbol,
        float $volume,
        float $price,
        float $stopLoss = 0.0,
        float $takeProfit = 0.0,
        int $type = self::TYPE_OPEN,
        ?\DateTime $expiration = null,
        int $offset = 0,
        string $customComment = '',
        int $order = 0
    ) {
        $tz = new \DateTimeZone( 'Europe/Berlin' );

        $expiration = $expiration ?? new \DateTime('now');

        $this->cmd = $cmd;
        $this->customComment = $customComment;
        $this->expiration = (clone $expiration)->setTimezone( $tz )->getTimestamp() * 1000;
        $this->offset = $offset;
        $this->order = $order;
        $this->price = $price;
        $this->sl = $stopLoss;
        $this->symbol = $symbol;
        $this->tp = $takeProfit;
        $this->type = $type;
        $this->volume = $volume;
    }

    public function jsonSerialize()
    {
        return get_object_vars( $this );
    }
}
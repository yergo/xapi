<?php
declare(strict_types=1);

namespace Yergo\Xapi;

class Command implements CommandInterface
{
    /**
     * @var string
     */
    protected string $command;

    /**
     * @var array|null
     */
    protected ?array $arguments;

    protected array $properties = [];

    public function __construct( string $command, ?array $arguments = null )
    {
        $this->command = $command;
        $this->arguments = $arguments;
    }

    public function withExtra( $name, $value ): self
    {
        $clone = clone $this;
        $clone->properties[$name] = $value;

        return $clone;
    }

    public function jsonSerialize(): array
    {
        $cmd = [
            'command'     => $this->command,
        ];

        if ( $this->arguments !== null ) {
            $cmd[ 'arguments' ] = $this->arguments;
        }

        if ($this->properties) {
            $cmd = array_merge($cmd, $this->properties);
        }

        return $cmd;
    }
}
# xApi wrapper - XTB api wrapper

## Install

```
composer require yergo/xapi v0.1-beta
```

in php file

```php
include "vendor/autoload.php";
```

## Components

We have two Api components, one for RESTy API connection, second one for stream connection.

Both are built around PHP socket streams and configured non-blocking.

To manage CPU usage by loops, please change $_BLOCK value in each Api component

```php
Yergo\Xapi\StreamClient::$_BLOCK   =   1000; // 1ms block
Yergo\Xapi\ApiClient::$_BLOCK      = 100000; // 100ms block
```

### Api Connection

```php
$api = new Yergo\Xapi\ApiClient();
```

This component is for logging into platform, obtain session
identification for stream and manage trades.

### Stream Connection

```php
$streamApi = new Yergo\Xapi\StreamClient();
```

This component listens to the stream of data you are subcribed for. This gives
a real-time feeling of obtaining current trades, prices and news.

### Commands

This is possible to use `Command` component to pass commands to both Api components.

```php
$cmd = new Yergo\Xapi\Command('getIbsHistory', [
    'start' => new DateTime('-1h'),
    'end' => new DateTime('now'),
]);
```

Commands available for each api are documented in http://developers.xstore.pro/documentation/

You can also use `Yergo\Xapi\CommandFactory` to obtain `Command` objects in less letters:

```php
use Yergo\Xapi\CommandFactory as c;

$cmd = c::getIbsHistory(new DateTime('-1h'), new DateTime('now'));
```

## Example
Refer to [example](example.php) for more information.
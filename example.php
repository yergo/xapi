<?php
declare(strict_types=1);

use Yergo\Xapi as Xapi;
use Yergo\Xapi\CommandFactory as c;

set_time_limit( 0 );

include "vendor/autoload.php";

$LOGIN    = '12345678910';
$PASSWORD = 'password';

// Instantiate and connect API
$api = new Xapi\ApiClient();
$api->connect();

// Obtain stream session ID
['streamSessionId' => $streamSessionId] = $api->execute(c::login($LOGIN, $PASSWORD));

// Instantiate stream API
$stream = new Xapi\StreamClient($streamSessionId);
$stream->connect();

// define ticker functions
$ticker = function($data) { print_r($data); };

$stream->setTicker($stream::TICK_PRICE, $ticker);

$stream->subscribe($stream::TICK_PRICE, 'BITCOIN');
$stream->subscribe($stream::TICK_PRICE, 'USDCAD');


// begin listening
$stream->listen();
